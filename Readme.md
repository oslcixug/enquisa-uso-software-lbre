# Enquisa de uso de software libre

## Con esta enquisa queremos saber o grao de coñecemento que a comunidade universitaria galega ten sobre o software libre, co obxectivo de deseñar e promover canles e accións que faciliten o seu uso.

Esta iniciativa enmárcase no Plan de Acción de Software Libre 2023 da Xunta de Galicia, dentro das liñas de actuación para mellorar e potenciar os mecanismos de formación e difusión do software de fontes abertas entre a cidadanía galega e en particular no estudando universitario.

[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

Os contidos deste repositorio están dispoñibles baixo unha licenza
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
---
![entidades amtega e cixug](imaxes/logo_amtega_cixug.png)

