data <- read.csv("survey_177235_R_data_file.csv", quote = "'\"", na.strings=c("", "\"\""), stringsAsFactors=FALSE, fileEncoding="UTF-8-BOM")


# LimeSurvey Field type: F
data[, 1] <- as.numeric(data[, 1])
attributes(data)$variable.labels[1] <- "id"
names(data)[1] <- "id"
# LimeSurvey Field type: DATETIME23.2
data[, 2] <- as.character(data[, 2])
attributes(data)$variable.labels[2] <- "submitdate"
names(data)[2] <- "submitdate"
# LimeSurvey Field type: F
data[, 3] <- as.numeric(data[, 3])
attributes(data)$variable.labels[3] <- "lastpage"
names(data)[3] <- "lastpage"
# LimeSurvey Field type: A
data[, 4] <- as.character(data[, 4])
attributes(data)$variable.labels[4] <- "startlanguage"
names(data)[4] <- "startlanguage"
# LimeSurvey Field type: A
data[, 5] <- as.character(data[, 5])
attributes(data)$variable.labels[5] <- "seed"
names(data)[5] <- "seed"
# LimeSurvey Field type: DATETIME23.2
data[, 6] <- as.character(data[, 6])
attributes(data)$variable.labels[6] <- "startdate"
names(data)[6] <- "startdate"
# LimeSurvey Field type: DATETIME23.2
data[, 7] <- as.character(data[, 7])
attributes(data)$variable.labels[7] <- "datestamp"
names(data)[7] <- "datestamp"
# LimeSurvey Field type: A
data[, 8] <- as.character(data[, 8])
attributes(data)$variable.labels[8] <- "Acabas de chegar á universidade?"
data[, 8] <- factor(data[, 8], levels=c("AO01","AO02","AO03"),labels=c("Si, estou en primeiro curso", "Non, estou en cursos intermedios", "Non, estou no último curso"))
names(data)[8] <- "G01Q02"
# LimeSurvey Field type: A
data[, 9] <- as.character(data[, 9])
attributes(data)$variable.labels[9] <- "En que rama do coñecemento estás estudando?"
data[, 9] <- factor(data[, 9], levels=c("AO01","AO02","AO03","AO04","AO05"),labels=c("Artes e humanidades", "Ciencias sociais e xurídicas", "Ciencias", "Ciencias da saúde", "Enxeñaría e arquitectura"))
names(data)[9] <- "G01Q03"
# LimeSurvey Field type: A
data[, 10] <- as.character(data[, 10])
attributes(data)$variable.labels[10] <- "Coñeces o concepto de software libre e as súas implicacións?"
data[, 10] <- factor(data[, 10], levels=c("AO01","AO02","AO03"),labels=c("Si, coñezo o concepto e as catro liberdades que ofrece", "Teño certa idea pero sen profundizar demasiado", "Non coñezo nada acerca do software libre"))
names(data)[10] <- "G02Q01"
# LimeSurvey Field type: A
data[, 11] <- as.character(data[, 11])
attributes(data)$variable.labels[11] <- "O software libre é gratuito?"
data[, 11] <- factor(data[, 11], levels=c("AO01","AO02","AO03"),labels=c("Si, sempre", "Non é necesario para ser libre", "Non sei"))
names(data)[11] <- "G02Q02"
# LimeSurvey Field type: A
data[, 12] <- as.character(data[, 12])
attributes(data)$variable.labels[12] <- "Se teño Windows ou MacOS no meu ordenador, podo instalar aplicacións con licenza libre?"
data[, 12] <- factor(data[, 12], levels=c("AO01","AO02","AO03"),labels=c("Non, só podo instalar software de pago", "Si, hai moito software libre para estas plataformas", "Non sei"))
names(data)[12] <- "G02Q03"
# LimeSurvey Field type: A
data[, 13] <- as.character(data[, 13])
attributes(data)$variable.labels[13] <- "Unha licenza libre permite facer copias, pero podo vendelas?"
data[, 13] <- factor(data[, 13], levels=c("AO01","AO02","AO03"),labels=c("Non se poden vender", "Si, as licenzas libres non o impiden", "Non sei"))
names(data)[13] <- "G02Q04"
# LimeSurvey Field type: A
data[, 14] <- as.character(data[, 14])
attributes(data)$variable.labels[14] <- "Para facer os teus traballos, que paquete ofimático soes empregar?"
data[, 14] <- factor(data[, 14], levels=c("AO01","AO02","AO03"),labels=c("Microsoft Office", "LibreOffice / OpenOffice", "Outro"))
names(data)[14] <- "G03Q01"
# LimeSurvey Field type: A
data[, 15] <- as.character(data[, 15])
attributes(data)$variable.labels[15] <- "Cal é o teu navegador web favorito?"
data[, 15] <- factor(data[, 15], levels=c("AO01","AO02","AO03","AO04","AO05"),labels=c("Mozilla Firefox", "Google Chrome", "Microsoft Edge", "Safari", "Outro"))
names(data)[15] <- "G03Q02"
# LimeSurvey Field type: A
data[, 16] <- as.character(data[, 16])
attributes(data)$variable.labels[16] <- "[LibreOffice/OpenOffice] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 16] <- factor(data[, 16], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[16] <- "G03Q03_SQ001"
# LimeSurvey Field type: A
data[, 17] <- as.character(data[, 17])
attributes(data)$variable.labels[17] <- "[Mozilla Firefox] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 17] <- factor(data[, 17], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[17] <- "G03Q03_SQ002"
# LimeSurvey Field type: A
data[, 18] <- as.character(data[, 18])
attributes(data)$variable.labels[18] <- "[DuckDuckGo] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 18] <- factor(data[, 18], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[18] <- "G03Q03_SQ003"
# LimeSurvey Field type: A
data[, 19] <- as.character(data[, 19])
attributes(data)$variable.labels[19] <- "[VLC Media Player] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 19] <- factor(data[, 19], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[19] <- "G03Q03_SQ004"
# LimeSurvey Field type: A
data[, 20] <- as.character(data[, 20])
attributes(data)$variable.labels[20] <- "[Jitsi Meet] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 20] <- factor(data[, 20], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[20] <- "G03Q03_SQ005"
# LimeSurvey Field type: A
data[, 21] <- as.character(data[, 21])
attributes(data)$variable.labels[21] <- "[Gimp] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 21] <- factor(data[, 21], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[21] <- "G03Q03_SQ006"
# LimeSurvey Field type: A
data[, 22] <- as.character(data[, 22])
attributes(data)$variable.labels[22] <- "[Inkscape] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 22] <- factor(data[, 22], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[22] <- "G03Q03_SQ007"
# LimeSurvey Field type: A
data[, 23] <- as.character(data[, 23])
attributes(data)$variable.labels[23] <- "[Audacity] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 23] <- factor(data[, 23], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[23] <- "G03Q03_SQ008"
# LimeSurvey Field type: A
data[, 24] <- as.character(data[, 24])
attributes(data)$variable.labels[24] <- "[OBS Studio] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 24] <- factor(data[, 24], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[24] <- "G03Q03_SQ009"
# LimeSurvey Field type: A
data[, 25] <- as.character(data[, 25])
attributes(data)$variable.labels[25] <- "[F-Droid] Cales das seguintes aplicacións libres coñeces e/ou usas?"
data[, 25] <- factor(data[, 25], levels=c("AO01","AO04","AO03"),labels=c("Coñezo e uso", "Coñezo pero nunca usei", "Non coñezo"))
names(data)[25] <- "G03Q03_SQ010"
# LimeSurvey Field type: A
data[, 26] <- as.character(data[, 26])
attributes(data)$variable.labels[26] <- "Indica a continuación outras aplicacións libres que coñezas e uses..."
names(data)[26] <- "G03Q10"
# LimeSurvey Field type: A
data[, 27] <- as.character(data[, 27])
attributes(data)$variable.labels[27] <- "E por último, queres deixarnos algún comentario?"
names(data)[27] <- "G04Q11"
